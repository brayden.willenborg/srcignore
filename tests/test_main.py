#
#  Copyright 2024 Calian Ltd. All rights reserved.
#

from functools import partial
from unittest.mock import Mock, patch

from srcignore.main import ls


def test_ls(capsys) -> None:
    with patch(
        "srcignore.main.parse_gitignore",
        partial(lambda *_: partial(lambda name: "ignore" in name)),
    ):
        path_mock = Mock()
        path_mock.rglob.return_value = ["somefile.py", ".srcignore"]
        ls("randomdir", path_mock)
        captured = capsys.readouterr()
        assert ".srcignore" in captured.out
        assert "somefile.py" not in captured.out
